###############################################################################
#           CONSTRUCTION DU DIAGRAMME DE VENN PROPORTIONNEL
###############################################################################
# Description des objets importants :
#       Input (recupere de lexico.py) :
#           - Graphe primal materialise par LexicoMap()
#           - fonction de poids W
#       Output :
#           - une liste "Polygons" de n "Polygon" qui sont les n ensembles du 
#             diagramme de Venn.
#       Attention : sur le trace on dirait qu'il y a des arcs de cercle, en 
#       realite en zoomant on voit que se sont bien des segments.
###############################################################################


from lexico import*
import copy
import pickle
from math import sqrt, pi, cos, sin

def Uniform():
    W = [[(n-i)*Binom(n/2,n) for j in range(Binom(i+1,n))] for i in range(n-1)]+[[1]]
    return W
    

class Point:
    
    """ Classe definie par :
        - layout : numero de la couche
        - j : position angulaire dans le sens trigonometrique
        - color : couleur (initialement [1,2,4] est possible par ex, 
            au moment du trace il faut qu'il n'y ait qu'une seule couleur)"""
            
    def __init__(self, layer=0, j=0, col=[], pointEnd=False,ray=None):
        self.layer = layer
        self.j = j
        self.color = col
        self.pointEnd = pointEnd
        self.ray = ray
    
    def r(self):
        if self.ray != None:
            return self.ray
        else:
            r0 = W[n-1][0]**0.5
            if self.layer==0:
                return r0
            else:            
                j_ = int(self.j)
                if self.pointEnd:
                    j_-=1
                j_ = j_%len(W[n-self.layer-1])
                R = (Binom(n/2,n)*(W[n-self.layer-1][j_]-(1-1/float(Binom(n/2,n)))*r0**2))**0.5
                return R
    def arg(self):
        j_ = self.j   
        return j_*2*math.pi/float(Binom(n/2,n))
    
    def shift_j(self, incr, pEnd=False):
        return Point(self.layer, j = self.j + incr, col=self.color, pointEnd=pEnd, ray=self.ray)
    
    def x(self):
        return cmath.rect(self.r(),self.arg()).real
        
    def y(self):
        return cmath.rect(self.r(),self.arg()).imag
    
    def link(p1, p2):
        color = list(set(p1.color) & set(p2.color))
        if color == []:
            print("Erreur : les deux points n'ont pas de couleur en commun.")
        
class Layer:
    
    """ Classe definie par : 
        - son numero k
        Avec ce numero et LexicoMap(), on determine une liste initiale de points,
        de longueur n pour 0 < k < n-1 et de longueur Binom(n/2,n) pour k = 0 ou n-1.
        Chacun de ces points est le point de reference, et points0 et pointsEnd sont 
        les points qu on va deplacer pour relier les couches entre elles."""
        
    def __init__(self, numero):
        self.k = numero
        self.points = []
        M = LexicoMap()
        if numero == 0:
            self.len = Binom(n/2,n)
            self.points = [Point(numero,j,list(set([k for k in range(1,n+1)])-set(M[-1][j]))) for j in range(len(M[-1]))]
            for i in range(n-2,0,-1):
                for j in range(len(M[i])):
                    if i<len(M)-1 and j>=len(M[i+1]):
                        self.points.append(Point(numero,j,list(set([k for k in range(1,n+1)])-set(M[i][j]))))
            
        elif numero == n-1:
            self.len = Binom(n/2,n)
            self.points = [Point(numero,j,[j+1]) for j in range(len(M[0]))]

        else:
            for j in range(len(M[-numero])):
                if j<len(M[-numero-1]):
                    self.points.append(Point(numero,j,list(set(M[-numero][j])-set(M[-numero-1][j]))))
                else:
                    self.points.append(Point(numero,j,M[-numero][j]))
            self.len = len(self.points)
            
class Polygon:
    """Classe definie par la liste des sommets du polygone
    et le nombre de sommets"""
    def __init__(self,sommets=[]):
        self.sommets = sommets
        self.size = len(sommets)
    
    def addEdge(self,P,pos):
        self.sommets.insert(pos,P)
    
    def popEdge(self,pos):
        return self.sommets.pop(pos)
    
    def Aire(self):
        A = 0
        for i in range(self.size):
            A += 0.5*(self.sommets[i][0]*self.sommets[(i+1)%self.size][1]
            - self.sommets[(i+1)%self.size][0]*self.sommets[i][1])
        return A
    
    def PinPoly(self,P):
        inside =False 
        x,y = P[0],P[1]
        p1x,p1y = self.sommets[0]
        for i in range(self.size+1):
            p2x,p2y = self.sommets[i % self.size]
            if y > min(p1y,p2y):
                if y <= max(p1y,p2y):
                    if x <= max(p1x,p2x):
                        if p1y != p2y:
                            xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xinters:
                            inside = not inside
            p1x,p1y = p2x,p2y
    
        return inside
    
    def Draw(self,color):
        for i in range(self.size):
            plt.plot([self.sommets[i][0],self.sommets[(i+1)%self.size][0]],
                     [self.sommets[i][1],self.sommets[(i+1)%self.size][1]],c=color,linewidth=1.5)

# parametrage pour pouvoir simuler des arcs de cercle
nbPoints = 4
if n<5:
    nbPoints = 12;
if n>=5 and n<7:
    nbPoints = 10
if n>=7 and n<10:
    nbPoints = 7

def drawVenn(ind=0):

    Layers = [Layer(k) for k in range(n)]
    
    Polygons = [Polygon([(0,0) for j in range(nbPoints*Binom(n/2,n))]) for i in range(n)]
    
    prev_areas = np.zeros(Binom(n/2,n)) 
    cur_areas = np.zeros(Binom(n/2,n))  
    r0 = sqrt(W[n-1][0]/pi)
    
    for k in range(n):  # Boucle sur les couches : de l'interieur vers l'exterieur
        for p in Layers[k].points: # Boucle sur les ensembles : parcours du cercle trigonometrique
            j = p.j                                                                         #
            if k==0:                                                                        #
                p.r = r0                                                                    #
            if k==1:                                                                        # Calcul de la distance
                cur_areas[j] = W[n-k-1][j] - pi*r0**2                                       # au centre a laquelle
                p.r = sqrt(Binom(n/2,n)*cur_areas[j]/pi + r0**2)                            # tracer la couche 
            if k>1:                                                                         # pour avoir la bonne
                A = W[n-k-1][j] - pi*r0**2                                                  # aire.
                for j_ in range(len(L[n-k])):                                               #
                    if j_!=j and (set(L[n-k][j_]) & set(L[n-k-1][j]) == set(L[n-k-1][j])):  #                    
                        A -= prev_areas[j_]                                                 #                                                           #
                p.r = sqrt(Binom(n/2,n)*A/pi + r0**2)                                       #
                cur_areas[j] = A                                                            #
            
            
            for c in p.color: # Possibilite qu'une frontiere soit commume a plusieurs ensembles
                p0 = Point(p.layer,j,[c],ray=p.r)
                
                if ind==0:                                                                  #
                    incr = 1/float(nbPoints-1)                                              #        
                    p1 = p0.shift_j(incr)                                                   #    
                else:                                                                       # On trace la portion
                    incr = 1/float(nbPoints-3)                                              # de couche a la bonne
                    p1 = p0                                                                 # distance au centre.
                Polygons[c-1].sommets[nbPoints*j+1] = p1.x(),p1.y()                         #
                for i in range(1,nbPoints-2):                                               #
                    pA,pB = p1.shift_j((i-1)*incr), p1.shift_j(i*incr,pEnd=(i==nbPoints-3)) #
                    pA.link(pB)                                                             #
                    Polygons[c-1].sommets[nbPoints*j+i+1] = pB.x(),pB.y()                   #
                
                pEnd = p0.shift_j(1,pEnd=True)
    
                for i in range(k-1,-1,-1):                                                  #        
                    if (j-1)%Binom(n/2,n) < Layers[i].len:                                  #
                        subPoint0 = Layers[i].points[(j-1)%Binom(n/2,n)]                    #
                        Color0 = list(set(p0.color) & set(subPoint0.color))                 #                  
                        if Color0 != []:                                                    # On cherche si on ne         
                            p0 = Point(subPoint0.layer,j,p0.color,ray=subPoint0.r)          # peut pas relier ce 
                                                                                            # qu'on vient de tracer
                    if (j+1)%Binom(n/2,n) < Layers[i].len:                                  # une couche inferieure.
                        subPointEnd = Layers[i].points[(j+1)%Binom(n/2,n)]                  #
                        ColorEnd = list(set(pEnd.color) & set(subPointEnd.color))           #
                        if ColorEnd != []:                                                  #                        
                            pEnd = Point(subPointEnd.layer,                                 #
                                         (j+1)%Binom(n/2,n),p0.color,ray=subPointEnd.r)     #
                                                                                            
                p0.link(p1)                                                                 #            
                Polygons[c-1].sommets[nbPoints*j] = p0.x(),p0.y()                           #            
                if ind==0:                                                                  # On relie.       
                    (p1.shift_j(1-2*incr)).link(pEnd)                                       #            
                else:                                                                       #
                    (p1.shift_j(1)).link(pEnd)                                              #
                    
                Polygons[c-1].sommets[nbPoints*(j+1)-1] = pEnd.x(),pEnd.y()
        prev_areas = np.array(cur_areas)
    return Layers,Polygons
