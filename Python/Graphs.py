from lexico import*

def Nodes(Map): # Noeuds du graphe associe a la liste des chaines
    # Les attributs sur les noeuds sont desormais des strings
    V = []
    dic = {}
    V.append(str([])) # ensemble vide 
    dic[str([])] = (0,n)
    V.append(str([k for k in range(1,n+1)])) # intersection de tous les elements
    dic[str([k for k in range(1,n+1)])] = (0,0)
    for i in range(len(Map)):
        for j in range(len(Map[i])):
            x = Map[i][j]
            V.append(str(x))
            dic[str(x)] = (j,n-1-i) 
    return (V,dic)

def Edges(M): # Arretes du graphe associe a la liste des chaines
    E = [(str([k for k in range(1,n+1)]),str(m)) for m in M[-1]]
    for k in range(n-2,0,-1):
        for i in range(len(M[k])):
            if i<len(M[k-1]): # on relie au successeur
                E.append((str(M[k][i]),str(M[k-1][i])))
            else: # on relie a l'ensemble vide
                E.append((str(M[k][i]),str([])))
            if k<len(M)-1 and i>=len(M[k+1]): # on relie a l'intersection de tous
                E.append((str([p for p in range(1,n+1)]),str(M[k][i])))
    for k in range(n):
        E.append((str([k+1]),str([])))
    return E


def LexicoGraph(display=False):
    G = pgv.AGraph()
    M = LexicoMap()
    (V,dic) = Nodes(M)
    #G.add_nodes_from(V)
    G.add_edges_from(Edges(M))
    if (display):
        G.layout(prog = 'dot')
        G.draw('planar.png')    
        plt.show()
    return G

def get_node_position(G,node_name):
    if not G.has_layout:
        print("error : graph has no layout")
        return
    s = G._run_prog()
    name = s.find(node_name)
    i = s[name:].find('pos=') + name
    guillemets_ouvrants = i+4
    virg = s[i:].find(',') + i
    guillemets_fermants = s[virg+1:].find(",") + virg
    x = s[guillemets_ouvrants+1:virg]
    y = s[virg+1:guillemets_fermants]
    x = float(x)
    y = float(y)
    return (x,y)
    
def get_edge_positions(G,node1_name,node2_name):
    if not G.has_layout:
        print("error : graph has no layout")
        return
    s = G._run_prog()
    edge_name = node1_name + '" -- "' + node2_name
    name = s.find(edge_name)
    i = s[name:].find('pos=') + name
    j = i+5
    coord = []
    maxi = s[j:].find(']') + j
    while 1:
        j_new= s[j:].find(' ') + j
        if j_new>=maxi:
            break
        x = float(s[j:s[j:].find(',')+j])
        y = float(s[s[j:].find(',')+j+1:j_new])
        coord.append((x,y))
        j = j_new+1
    return coord
